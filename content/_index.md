---
title: "Home"
date: 2019-04-19T14:32:36+01:00
page_heading: tech.
draft: false
menu:
    main:
        weight: 1
        name: Home
---
## Hi, I'm Jim.

I am a multidisciplinary software engineer. I have worked across the entire software stack, been responsible for leading small -
medium sized teams and have both run and scheduled projects using the agile methodology.

I take pride in the delivery of robust, pliable software projects.

Tightenupthe.tech is the vehicle by which I sell my services. There is a human behind this
entity and he is a massive geek. The content of this site is a work in progress and subject to change.

If you have technical opportunities or any questions about the content of this site, please feel free to reach out
via email at [jim@tightenupthe.tech](mailto:jim@tightenupthe.tech).
