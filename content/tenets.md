---
title: "Tenets"
date: 2019-04-19T14:32:36+01:00
page_heading: software.
draft: false
menu:
    main:
        weight: 2
---
I work to a series of tenets for robust software builds.

## Ten year software.

Code and software which will manage to hold the balance between craftmanship-quality and real pragmatism,
and be a delight to work with and extend, and will break the cycle of endless re-writes.

## Pliant code.

I will create software from code which doesn’t resist change, doesn’t resist learning, and instead
delivers value in the present and promises value in the future when adapted to meet new circumstances.

## Teams not Silos.

I will take a group of people with talents, specialisms, personal interests and career ambitions to forge a
unit greater than the sum of its parts. This means breaking down the barriers to communication and
collaboration, and building the respect and maturity necessary to enforce code quality and defend
rational decision making.

## House rules.

In respect to the principles of Agile, I put People before Process, and work with teams to create a
communication structure which is both appropriate for the people around the table, and is fine-tuned
to deliver the value to the operation. This spans standup, project management software, the key function
of the manager-on-foot, and peer review.

## Software sergeant-at-arms.

I enable conversations, encourage quality, but manage the alignment of software with value delivery to the
organisation. Work gets done, to a high quality, with a clear path to progression.
