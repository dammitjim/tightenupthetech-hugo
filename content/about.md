---
title: "About"
page_heading: knowledge.
date: 2019-05-12T19:57:22+01:00
draft: false
menu: footer
---

This website is built with the beautiful [Hugo](https://gohugo.io/) static site
generator and hosted on [Netlify](https://www.netlify.com/).

You can find the source code, along with my other personal projects, [here](https://gitlab.com/dammitjim/tightenupthetech-hugo).
