---
title: "Services"
date: 2019-04-19T14:32:36+01:00
page_heading: service.
draft: false
menu:
    main:
        weight: 3
---

## Delivery of complex software

In the delivery of software, I engage in pragmatic decision making, pragmatic working methodologies,
and pragmatic relationships. I look to quality of thinking, quality in software design, and quality
in the alignment of market needs or programme objectives with software.

There will always be technical debt, I aim to make it manageable so it can be paid back.

## Boots on the ground programming

The code I produce through my ten-year-software and pliant code principles
reduces the cost of maintenance of the code itself, and computation resources needed to
run the software. This too gives immediate benefit to the bottom line.

## Shaping Agile teams

Engineers as a group of professionals who take pride and responsibility for their work,
an engagement with users and a joy in delivery.

## Architecture, scoping and planning

I have produced high quality documents for presentation to key stakeholders of both the
technical and business variety. Data modelling, system architecture and communication have
historically been key parts of my role.
